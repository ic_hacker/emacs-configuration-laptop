# `Emacs`-Configuration-**Laptop**

# Introduction
    This repo is a all emacs configuration files of `Emacs`.

## Content

- Company mode
- Flycheck mode

- Lua mode

- Merlin
- Tuareg
- Utop

- Markdown mode
- YAML mode